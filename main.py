from kivy.app import App
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty, ListProperty, BooleanProperty, StringProperty
from kivy.vector import Vector
from kivy.clock import Clock
from random import randint, randrange, seed
import random
from kivy.utils import platform
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from math import fabs
from kivy.storage.jsonstore import JsonStore
from kivy.core.audio import SoundLoader
from kivy.config import Config
from kivy.uix.popup import Popup
from kivy.uix.relativelayout import RelativeLayout
from kivy.graphics import Ellipse, Color
Config.set('graphics','width','360')
Config.set('graphics','height','640')
from kivy.core.window import Window
from kivy.core.text import LabelBase
import time
Config.set('kivy', 'exit_on_escape', '1')

KIVY_FONTS = [
    {
        "name": "Adamhand",
        "fn_regular": "data/fonts/adamhand.ttf",
    }
]
for font in KIVY_FONTS:
    LabelBase.register(**font)


class ColorPongPaddle(Widget):
    """Describes Circle. bouncing functions and color check"""
    angle = NumericProperty(0) # each paddle every 40 degree
    color = ListProperty([0,0,0])
    paddlesize = NumericProperty(4 + Window.width/200)
    circlewidth = NumericProperty(Window.width/2 - (4 + Window.width/200))
    def bounce_ball(self, ball, score):
        """Check for collision between a ball and a circle"""
        # print(ball.center)
        # print(self.center)
        dx = ball.center_x + ball.radius - self.center_x
        dy = ball.center_y + ball.radius - self.center_y
        distance = Vector((ball.center_x + ball.radius, ball.center_y + ball.radius)).distance(self.center)
        if distance >= self.circlewidth - self.paddlesize - ball.radius :# circle radius - ball radius
            centerToBallAngle = Vector(0,1).angle((dx,dy))
            if centerToBallAngle <= 0: #angle 0-360 not 0-180 to  -180-0
                centerToBallAngle += 360
            # print("Odlegosc: ",distance)
            # print("Kat: ",centerToBallAngle)
            # print("Kat kola: ", self.angle%360)
            if self.checkcolor(ball, centerToBallAngle, self.angle%360):
                vx, vy = ball.velocity
                ball.angle = Vector(vx,vy).angle((dx,dy))
                bounced = Vector(vx,vy)
                if ball.angle > 90:
                    ball.angle-=90
                elif ball.angle < -90:
                    ball.angle+=90
                ball.angle = 180 - 2*ball.angle # set new angle off ball after bounce

                #ball bounce angle changing
                ball.touchspeed *= 3 # make changing angle a bit more aggresive
                if(ball.angle + ball.touchspeed > ball.minangle and ball.angle + ball.touchspeed < ball.maxangle):
                    ball.angle += ball.touchspeed
                else:
                    if(ball.angle + ball.touchspeed < ball.minangle):
                        ball.angle = ball.minangle
                    else:
                        ball.angle = ball.maxangle

                bounced = bounced.rotate(ball.angle) # finally, rotate the ball

                if score<10:
                    bounced *= 1.04 # make ball move faster
                elif score < 35:
                    bounced *= 1.01
                else:
                    bounced *= 1.001
                ball.velocity = bounced.x , bounced.y
                ball.touchspeed=0
                return True # add +1 to score
            else:
                return False

        else:
            return None

    def checkcolor(self, ball, ballCenterAngle, CircleAngle):
        angle = 360/ball.quantityofcolors
        for i in range(0,ball.quantityofcolors):
            # print("Ball angle: ", ballCenterAngle)
            # print("Kat poczatkowy",((angle*i)+CircleAngle)%360)
            # print("Kat koncowy",((angle*(i+1))+CircleAngle)%360,"\n------------\n")
            # specjalny warunek dla katu pilki na granicy przeskoku kata gdy roznica
            # miedzy sprawdzanymi katmi jest inna niz wielkosc kata wycinka okregu
            # jest to przypadek gdy jestesmy na granicy 0-360 stopni w gornej czesci kola
            if abs(((angle*i)+CircleAngle)%360 - ((angle*(i+1))+CircleAngle)%360) != angle:
                if ballCenterAngle <= 360 and ballCenterAngle>=((angle*i)+CircleAngle)%360:
                    if ball.ballcolor == ball.colors[i]:
                        return True
                    else:
                        return False
                elif ballCenterAngle>=0 and ballCenterAngle<=((angle*(i+1))+CircleAngle)%360:
                    if ball.ballcolor == ball.colors[i]:
                        return True
                    else:
                        return False
            if ballCenterAngle>=((angle*i)+CircleAngle)%360 and ballCenterAngle<=((angle*(i+1))+CircleAngle)%360:
                if ball.ballcolor == ball.colors[i]:
                    return True
                else:
                    return False
        return False


class ColorPongBall(Widget):
    """Describes ball color, velocity, how ball moves and color change"""
    bouncesound = SoundLoader.load('./sounds/pong.ogg')
    ballcolor = ListProperty([0,0,0])
    colors=[[0.7, 0, 1],[0, 0, 1],[0, 0.5, 0.5],[0, 1, 0],[1, 0.5, 0],[0.8, 0, 0]]
    quantityofcolors = NumericProperty(len(colors))
    angle = NumericProperty(0)
    maxangle = NumericProperty(300)
    minangle = NumericProperty(60)
    touchspeed = NumericProperty(0)
    velocity_x = NumericProperty(Window.width/400)
    velocity_y = NumericProperty(Window.width/400)
    velocity = ReferenceListProperty(velocity_x, velocity_y)
    radius = NumericProperty(5 + Window.width/50)#  ball size

    def __init__(self, *args, **kwargs):
        super(ColorPongBall, self).__init__(*args, **kwargs)
        self.changecolor()
        self.pos=(Window.width/2 - self.radius, Window.height/2 - self.radius)





    def move(self):
        self.pos = Vector(*self.velocity) + self.pos #  vel * - start speed

    def changecolor(self):
        random.seed()
        randomint = random.randrange(0,self.quantityofcolors)
        self.ballcolor = self.colors[randomint]


class ColorPongScreen(Screen):
    screenButtonSize = ListProperty(['40dp', '40dp'])
    bigfontsize = 100
    fontsize = 50
    score = NumericProperty(0)
    bestscore = NumericProperty(0)
    gameSounds = BooleanProperty(True)

    def __init__(self, *args, **kwargs):
        super(ColorPongScreen, self).__init__(*args, **kwargs)
        self.center = self.getCenter()

    def getCenter(self, offset = 0, mul = 1):
        return (mul*(Window.width/2)+ offset, mul*(Window.height/2) + offset)

    def platform_check(self):
        if platform() == 'android':
            import android
            android.map_key(android.KEYCODE_BACK, 1001)

        win = Window
        win.bind(on_keyboard=self.key_handler)

    def key_handler(self, window, keycode1, keycode2, text, modifiers):
        if keycode1 == 27 or keycode1 == 1001:
            content = QuitPopup(text='Do you want to quit?')
            self.popup = Popup(title='Dont leave me!', content=content,
                auto_dismiss=True, size_hint=(.5, .5))

            self.popup.open()

class GameScreen(ColorPongScreen):
    """Main game class"""
    ball = ObjectProperty(None)
    paddle = ObjectProperty(None)
    circleAndBallShift = NumericProperty(50)
    rotateSensivity = NumericProperty(50)
    paused = BooleanProperty(True)  # game paused or not

    def __init__(self, *args, **kwargs):
        super(GameScreen, self).__init__(*args, **kwargs)
        self.startDx = 0

    def serve_ball(self):
        self.score = 0
        # self.ball.center_x = self.center_x + self.circleAndBallShift # center of ball calculations
        # self.ball.center_y = self.center_y + self.circleAndBallShift
        self.ball.velocity = Vector(3,0).rotate(randint(0, 360))
        self.paddle.angle = randint(30,360)
        self.pause_play()

    def update(self, dt):
        self.ball.move()
        if(self.paddle.bounce_ball(self.ball,self.score) == True):
            if self.gameSounds:
                self.ball.bouncesound.play()
            self.score+=1
            if self.bestscore <= self.score:
                self.bestscore = self.score
                # self.store.put('bestscore', value=self.bestscore) #TODO
            self.ball.changecolor()
        elif (self.paddle.bounce_ball(self.ball,self.score) == False):
            self.pause_play()
            root_widget.current = 'endmenu_screen'
        else:
            pass



    def on_touch_move(self, touch):
        if touch.y < self.height / 3 and not self.paused:   # checks at 1/3 of screen (bottom)
            moving = int(touch.dx - touch.dsx/Window.width*10*(self.rotateSensivity/2+30)) #TODO zrobic zeby nie przeskakiwalo
            self.paddle.angle -= moving
            self.ball.touchspeed = moving

    def on_touch_up(self,touch):  # reset self.ball.touchspeed
        self.ball.touchspeed = 0

    def pause_play(self):
        # Button data:
        position = self.top
        if self.paused:
            Clock.schedule_interval(self.update, 1.0 / 60.0)
            self.paused = False
        else:
            Clock.unschedule(self.update)
            self.paused = True

    def playstopmusic(self,state):
        self.musicactive = state
        if self.musicactive:
            self.backgroundsound = SoundLoader.load('./sounds/background.wav')
            self.backgroundsound.loop = True
            self.backgroundsound.play()
        elif self.backgroundsound:
            self.backgroundsound.loop = False
            self.backgroundsound.stop()
            self.backgroundsound.unload()

    def quit(self):
        App.get_running_app().stop()


# class QuitPopup(RelativeLayout):
#     text = StringProperty('Are you sure you want to quit?')
#     def __init__(self,**kwargs):
#         self.register_event_type('on_answer')
#         super(QuitPopup,self).__init__(**kwargs)
#         print(game.score)
#     def on_answer(self, *args):
#         if args[0] == 'quit':
#             App.get_running_app().stop()
class HomeScreen(ColorPongScreen):
    pass

class EndMenuScreen(ColorPongScreen):
    pass

class SetttingsScreen(ColorPongScreen):
    pass

root_widget = Builder.load_file('main.kv')

class ColorPong(App):
    def build(self):
        return root_widget
    def switch_HomeScreen(self):
        root_widget.current = 'home_screen'

    def switch_GameScreen(self):
        root_widget.current = 'game_screen'

    def switch_EndMenuScreen(self):
        root_widget.current = 'endmenu_screen'

    def switch_SettingsScreen(self):
        root_widget.current = 'settings_screen'



if __name__ == '__main__':
    ColorPong().run()
